﻿using System;
using DMS.Base;
using DMS.Sound;

namespace Example
{
    public class Sound : Disposable
    {
        private AudioPlaybackEngine soundEngine;

        public Sound()
        {
            soundEngine = new AudioPlaybackEngine();
        }

        public void Shoot()
        {
            soundEngine.PlaySound(Resourcen.shoot44);
        }

        public void Goodie()
        {
            soundEngine.PlaySound(Resourcen.Goodie3);
        }

        public void Hit()
        {
            soundEngine.PlaySound(Resourcen.Hit);
        }

        public void BadGoodie()
        {
            soundEngine.PlaySound(Resourcen.BadGoodie3);
        }

        public void Background()
        {
            soundEngine.PlaySound(Resourcen.Background3); 
        }

        protected override void DisposeResources()
        {
            throw new NotImplementedException();
        }
    }
}
