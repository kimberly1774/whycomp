﻿using DMS.Geometry;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    class Ball
    {
        public Box2D box;
        public Vector2 pos;
        public Vector2 dir;
        public bool canCollide = true;

        public Ball(float X, float Y, float sizeX, float sizeY) {
            pos = new Vector2(X, Y);
            box = new Box2D(X, Y, sizeX, sizeY);
            dir = new Vector2(0.0f, 0f);
            
        }

        public void update()
        {
            box.X += dir.X;
            box.Y += dir.Y;

            if ((box.X < -1) | (box.MaxX > 1))
            {
                dir.X = dir.X * -1;
            }

            /*if ((box.Y < -1) | (box.MaxY > 1))
            {
                dir.Y = dir.Y * -1;
            }*/

            if (box.MaxX > 1.1f)
            {
                pos.X -= 0.011f;
                dir.X = dir.X * -1;

                
            }

            if ((box.MaxX < -1.015) | (box.MaxX > 1.015))
            {
                pos.X = 0;
                dir.X = dir.X * -1;
                Console.WriteLine("fix?");
                canCollide = true;
            }



            if (box.Y < -1)
            {
                pos.Y = 100;
            }
            if (box.Y > 1)
            {
                dir.Y = dir.Y * -1;
            }
        }



    }
}