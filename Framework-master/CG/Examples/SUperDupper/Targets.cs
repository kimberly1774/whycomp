﻿using DMS.Geometry;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Example
{
    class Targets
    {
        public Box2D box;
        public Targets(float X, float Y, float sizeX, float sizeY)
        {
            box = new Box2D(X, Y, sizeX, sizeY);

        }

    }
}
