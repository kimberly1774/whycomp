﻿using DMS.Application;
using DMS.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;
using System.Collections.Generic;
using DMS.OpenGL;
using System.Linq;
using DMS.Base;
using System.Threading;
using System.Diagnostics;



namespace Example
{

    class Controller
    {

        private Texture background;
        private Texture End;
        private Texture doubleMeTex;
        private Texture ballTex;
        private Texture startAni;
        private Texture youWonTex;
        private Texture startTex;
        private Texture playerTex;
        private Texture helpBallTex;

        private Texture fasterTex;
        private Texture growTex;
        private Texture removeColumnTex;
        private Texture moreRowsTex;

        private TextureFont font;

        private AnimationTextures eiscreme;
        private Stopwatch timeSource = new Stopwatch();

        private bool withTimeAdder = true;
        private bool jeez = true;
        private bool initialized = false;
        private bool lost = false;
        private bool hasNotStarted = false;
        private bool bugFix1 = false;
        private bool won = false;
        private bool timeStop = true;
        private bool withTime = false;
        private bool withPoints = false;
        private bool extraPoints = true;
        private bool showExtraPoints = false;
        private bool certainGoodie = false; 

        public int addBadGoodiesActivID;

        private Ball ball1;
        private Ball helpBall;

        public Sound sound;

        private Box2D player;
        private List<Box2D> hitable;
        private List<Box2D> goodieDoubleMe;

        private List<Box2D> goodieGrow;
        private List<Box2D> addBadGoodies;
        private List<Box2D> goodieMoreRows;
        private List<Box2D> goodieRemoveColumn;
        private List<Box2D> animatedHits;

        private List<Ball> helpBallList;

        private Box2D doubleMe;
        private Box2D theEnd;
        private Box2D grow;
        private Box2D faster;
        private Box2D moreRows;
        private Box2D animateMe;


        private Ball helpInList;
        private Box2D removeColumn;



        private float upperEdge = -0.15f;
        private float countHitablei = 1;
        private float countHitablej = 20f;
        private float ball1Size = 0.1f;
        private float bigball1Size = 0.2f;
        private float onScreenFontSize = .15f;
        private float onScreenFontPlace = .83f;

        private int countDoubleMe = 5;

        private int countUp = 0;
        private int countUpMax = 1;
        public float hittablesPosX = -1;
        //public float hittablesPosY = 0.9f;


        public int hitpoint = 0;

        private static float aspect = 0.7f;
        public float hittablesPosY = 1.0f - (0.1f * aspect);

        public event EventHandler OnShoot;
        public event EventHandler OnBadGoodie;
        public event EventHandler OnGoodie;
        public event EventHandler OnHit;
        public event EventHandler OnBackground;

        public static ExampleApplication app;
        public static Controller controller;

        private Stopwatch Timer;
        private TimeSpan newTime;
        private TimeSpan currentTime;

        private void init()
        {
            //if (hasNotStarted == true) {

            

            background = TextureLoader.FromBitmap(Resourcen.Background);
            youWonTex = TextureLoader.FromBitmap(Resourcen.Won);
            startTex = TextureLoader.FromBitmap(Resourcen.Start);
            playerTex = TextureLoader.FromBitmap(Resourcen.LeisteUnten);
            helpBallTex = TextureLoader.FromBitmap(Resourcen.Boy);
            End = TextureLoader.FromBitmap(Resourcen.LeisteUnten);
            ballTex = TextureLoader.FromBitmap(Resourcen.Boy2);
            startAni = TextureLoader.FromBitmap(Resourcen.Eis1);
            doubleMeTex = TextureLoader.FromBitmap(Resourcen.Kirsche);
            fasterTex = TextureLoader.FromBitmap(Resourcen.Karotte);
            growTex = TextureLoader.FromBitmap(Resourcen.Waffel2);
            removeColumnTex = TextureLoader.FromBitmap(Resourcen.Schoko);
            moreRowsTex = TextureLoader.FromBitmap(Resourcen.Tomate);
            font = new TextureFont(TextureLoader.FromBitmap(Resourcen.EiscremeFont), 10, 32, .8f, 1, .7f);

            eiscreme = new AnimationTextures(1f);

            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis1));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis2));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis3));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis4));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis5));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis6));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis7));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis8));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis9));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis10));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis11));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis12));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis13));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis14));
            eiscreme.AddFrame(TextureLoader.FromBitmap(Resourcen.Eis15));

            //timeSource.Start();

            ball1 = new Ball(0, -0.9f, ball1Size, ball1Size * aspect);
            ball1.dir.Y = -0.03f;
            player = new Box2D(0.0f, -0.95f, 0.6f, 0.015f);
            hitable = new List<Box2D>();
            theEnd = new Box2D(-1, -1.05f, 2, 0.1f);

            goodieDoubleMe = new List<Box2D>();
            goodieGrow = new List<Box2D>();
            addBadGoodies = new List<Box2D>();
            goodieMoreRows = new List<Box2D>();
            goodieRemoveColumn = new List<Box2D>();
            helpBallList = new List<Ball>();
            animatedHits = new List<Box2D>();

                sound = new Sound();


            Timer = new Stopwatch();


                OnBackground?.Invoke(this, null);

            hittablesPosY = upperEdge + hittablesPosY;
            
            
                for (int i = 0; i < countHitablei; i++)
                {
                    for (int j = 0; j < countHitablej; j++)
                    {
                        float breitebox = (2 / countHitablej);
                        float hoehebox = breitebox * aspect;
                        Box2D Targets = new Box2D(hittablesPosX + breitebox * j, hittablesPosY + hoehebox * i, breitebox, hoehebox);
                    
                    

                    hitable.Add(Targets);

                }
            }
            

            

            for (int i = 0; i < countDoubleMe; i++)
            {
                int Min = 1;
                int Max = hitable.Count;
                Random randNum = new Random();

                int[] spaceAssign = new int[countDoubleMe];

                for (int j = 0; j < spaceAssign.Length; j++)
                {
                    spaceAssign[j] = randNum.Next(Min, Max);
                }
                float a = hitable[spaceAssign[i]].X;
                float b = hitable[spaceAssign[i]].Y;


                Random rnd = new Random();
                //int whichGoodie = rnd.Next(1, 6);

                int whichGoodie = 1;

                if (whichGoodie == 1)
                {
                    doubleMe = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieDoubleMe.Add(doubleMe);
                }

                if (whichGoodie == 2)
                {
                    grow = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieGrow.Add(grow);
                }
                if (whichGoodie == 3)
                {
                    faster = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    addBadGoodies.Add(faster);
                }
                if (whichGoodie == 4)
                {
                    moreRows = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieMoreRows.Add(moreRows);
                }
                if (whichGoodie == 5)
                {
                    removeColumn = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieRemoveColumn.Add(removeColumn);
                }

                hitable.RemoveAt(spaceAssign[i]);

            }
            initialized = true;
            //}
        }

       


    private void changeDir()
        {
            ball1.canCollide = true;
        }

        private void superCollide()
        {

            ball1.dir.Y = ball1.dir.Y * (-1);
            ball1.dir.X = ball1.dir.X * (-1);
        }

        private void superCollideElse()
        {
            ball1.dir.Y = ball1.dir.Y * (-1);
            ball1.dir.X = ball1.dir.X * (-1);

        }

        private void setDown()
        {
            foreach (Box2D aPart in hitable)
            {
                aPart.Y -= 0.1f * aspect;
            }
            foreach (Box2D aPart in goodieDoubleMe)
            {
                aPart.Y -= 0.1f * aspect;
            }
            foreach (Box2D aPart in goodieGrow)
            {
                aPart.Y -= 0.1f * aspect;
            }
            foreach (Box2D aPart in addBadGoodies)
            {
                aPart.Y -= 0.1f * aspect;
            }
            foreach (Box2D aPart in goodieMoreRows)
            {
                aPart.Y -= 0.1f * aspect;
            }
            foreach (Box2D aPart in goodieRemoveColumn)
            {
                aPart.Y -= 0.1f * aspect;
            }
        }

        private void addGoodies0()
        {
            for (int j = 0; j < countHitablej; j++)
            {
                float breitebox = (2 / countHitablej);
                Box2D Targets = new Box2D(hittablesPosX + breitebox * j, hittablesPosY, breitebox, breitebox * aspect);
                hitable.Add(Targets);
                hitable.LastIndexOf(Targets);
            }
        }

        private void addGoodies()
        {
            addGoodies0();

            int counter = 0;
            Random reend = new Random();

            while (counter < 3)
            {
                counter++;
                int me = reend.Next(0, hitable.Count);

                float a = hitable[me].X;
                float b = hitable[me].Y;
                hitable.RemoveAt(me);

                Random rund = new Random();

                int whichGoodie = rund.Next(1, 6);

                if (certainGoodie == true)
                {
                    whichGoodie = 4;
                    Console.WriteLine(whichGoodie);
                } else
                {
                    whichGoodie = rund.Next(1, 6);
                }
                

                if (whichGoodie == 1)
                {
                    doubleMe = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieDoubleMe.Add(doubleMe);
                }

                if (whichGoodie == 2)
                {
                    grow = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieGrow.Add(grow);
                }
                if (whichGoodie == 3)
                {
                    faster = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    addBadGoodies.Add(faster);
                }
                if (whichGoodie == 4)
                {
                    moreRows = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieMoreRows.Add(moreRows);
                }
                if (whichGoodie == 5)
                {
                    removeColumn = new Box2D(a, b, 0.1f, 0.1f * aspect);
                    goodieRemoveColumn.Add(removeColumn);
                }
            }

        }



        private void Update(float updatePeriod)
        {
            if (lost == false)
            {

                if (withTime == true)
                {
                    if (withTimeAdder == true)
                    {
                        for (int i = 0; i < countHitablei; i++)
                        {
                            for (int j = 0; j < countHitablej; j++)
                            {
                                float breitebox = (2 / countHitablej);
                                float hoehebox = breitebox * aspect;
                                Box2D Targetss = new Box2D(hittablesPosX + breitebox * j, hittablesPosY + hoehebox * i + -hoehebox, breitebox, hoehebox);
                                hitable.Add(Targetss);
                            }

                        }
                        addGoodies();

                        withTimeAdder = false;
                    }
                }



                if (initialized == false)
                {
                    init();
                }

                

                if (Keyboard.GetState()[Key.Left])
                {
                    player.X -= updatePeriod * 1.4f;
                }
                else if (Keyboard.GetState()[Key.Right])
                {
                    player.X += updatePeriod * 1.4f;
                }

                TimeSpan remainingTime = new TimeSpan(0, 0, 0, 30);
                currentTime = remainingTime.Subtract(Timer.Elapsed);

                if (ball1.box.Intersects(player))
                {
                    if (withTime == true)
                    {
                        setDown();
                        addGoodies();
                    }

                    ball1.canCollide = true;
                    float differ = (player.CenterX - ball1.box.CenterX);
                    float playerhalf = player.SizeX * 0.5f;
                    float anglestrength = 0.025f;
                    float ratio = differ / playerhalf;
                    float dx = ratio * anglestrength * (-1);
                    float dy = ball1.dir.Y * (-1);

                    ball1.dir.X = dx;
                    ball1.dir.Y = 0.03f;

                    ball1.box.SizeX = ball1Size;
                    ball1.box.SizeY = ball1Size * aspect;


                    OnShoot?.Invoke(this, null);
                    showExtraPoints = false;

                    if (withPoints == true)
                    {
                        countUp += 1;
                        if (countUp == countUpMax)
                        {

                            setDown();
                            addGoodies();
                            countUp = 0;


                        }
                    }
                    

                }
                if (helpBall != null) // falls Helpball existiert
                {
                    if (helpBall.box.Intersects(player))
                    {


                        helpBall.canCollide = false;
                        float differ = (player.CenterX - helpBall.box.CenterX);
                        float playerhalf = player.SizeX * 0.5f;
                        float anglestrength = 0.025f;

                        float ratio = differ / playerhalf;
                        helpBall.dir.X = ratio * anglestrength * (-1);
                        helpBall.dir.Y = helpBall.dir.Y * (-1);
                        helpBall.canCollide = true;
                    }


                }
                for (int i = 0; i < helpBallList.Count; i++)
                {
                    if (helpBallList[i].box.Intersects(player))
                    {
                        helpBallList[i].canCollide = false;

                        float differ = (player.CenterX - helpBallList[i].box.CenterX);
                        float playerhalf = player.SizeX * 0.5f;
                        //float anglestrength = 0.02f;

                        float ratio = differ / playerhalf;

                        helpBallList[i].dir.X *= -1;
                        //helpBall.dir.X = ratio * anglestrength * (-1);
                        helpBallList[i].dir.Y = helpBallList[i].dir.Y * (-1);
                        helpBallList[i].canCollide = true;
                    }
                }

                /*if (ball1.box.Intersects(doubleMe) && (helpBall == null)) // wenn helpball nicht existiert
                {
                    helpBall = new Ball(0, 0, 0.025f, 0.025f);

                    helpBall.dir.Y = 0.03f;
                }*/



                for (int i = 0; i < goodieDoubleMe.Count; i++)
                {
                    if (goodieDoubleMe[i].Intersects(ball1.box) && (ball1.canCollide == true))
                    {
                        if (goodieDoubleMe[i].Y < ball1.box.Y && goodieDoubleMe[i].X > ball1.box.X)
                        {
                            superCollide();
                        }
                        else
                        {
                            superCollideElse();
                        }

                        OnGoodie?.Invoke(this, null);
                        goodieDoubleMe.RemoveAt(i);
                        i--;
                        //ball1.canCollide = false; what
                        helpBall = new Ball(0, 0, 0.07f, 0.07f * aspect);

                        Random roond = new Random();
                        int mel = roond.Next(0, 4);
                        if (mel == 0)
                        {
                            helpBall.dir.X = 0.01f;
                        }
                        if (mel == 1)
                        {
                            helpBall.dir.X = -0.01f;
                        }
                        if (mel == 2)
                        {
                            helpBall.dir.X = -0.0075f;
                        }
                        if (mel == 3)
                        {
                            helpBall.dir.X = 0.0075f;
                        }

                        helpBall.dir.Y = 0.03f;

                        if (helpBall != null)
                        {
                            helpInList = new Ball(0, 0, 0.07f, 0.07f * aspect);
                            helpInList.dir.Y = 0.03f;
                            Random rnd = new Random();
                            int me = rnd.Next(0, 4);
                            if (me == 0)
                            {
                                helpInList.dir.X = 0.01f;
                            }
                            if (me == 1)
                            {
                                helpInList.dir.X = -0.01f;
                            }
                            if (me == 2)
                            {
                                helpInList.dir.X = -0.0075f;
                            }
                            if (me == 3)
                            {
                                helpInList.dir.X = 0.0075f;
                            }



                            helpBallList.Add(helpInList);
                        }
                    }


                }

                for (int i = 0; i < addBadGoodies.Count; i++)
                {
                    if (addBadGoodies[i].Intersects(ball1.box) && ball1.dir.Y == -0.03f)
                    {
                        OnBadGoodie?.Invoke(this, null);

                        /*ball1.dir.Y = 0.07f;
                        bugFix1 = true;*/
                        addBadGoodiesActivID = i;
                        certainGoodie = true; 
                        addGoodies();

                        if (addBadGoodies[i].Y < ball1.box.Y && addBadGoodies[i].X > ball1.box.X)
                        {
                            superCollide();
                        }
                        else
                        {
                            superCollideElse();
                        }
                        addBadGoodies.RemoveAt(addBadGoodiesActivID);
                    }
                    /*ball1.canCollide = true;


                    if (ball1.box.Intersects(player) && bugFix1 == true)
                    {

                        bugFix1 = false;
                    }*/
                }

                for (int i = 0; i < addBadGoodies.Count; i++)
                {

                    if (addBadGoodies[i].Intersects(theEnd) && extraPoints == true)
                    {
                        hitpoint = hitpoint + 5;
                        extraPoints = false;
                        showExtraPoints = true;
                    }
                }


                for (int i = 0; i < goodieMoreRows.Count; i++)
                {
                    if (goodieMoreRows[i].Intersects(ball1.box))
                    {

                        OnBadGoodie?.Invoke(this, null);


                        for (int k = 0; k < 1; k++)
                        {
                            ball1.canCollide = false;
                            setDown();
                            for (int j = 0; j < countHitablej; j++)
                            {
                                Box2D Targets = new Box2D(-1 + 0.1f * j, hittablesPosY, 0.1f, 0.1f * aspect);
                                hitable.Add(Targets);
                            }
                        }
                        if (goodieMoreRows[i].Y < ball1.box.Y && goodieMoreRows[i].X > ball1.box.X)
                        {
                            superCollide();
                        }
                        else
                        {
                            superCollideElse();
                        }
                        goodieMoreRows.RemoveAt(i);
                    }

                    ball1.canCollide = true;

                }

                for (int i = 0; i < goodieMoreRows.Count; i++)
                {

                    if (goodieMoreRows[i].Intersects(theEnd) && extraPoints == true)
                    {

                        hitpoint = hitpoint + 5;
                        extraPoints = false;
                        showExtraPoints = true;
                    }

                }


                    for (int i = 0; i < goodieGrow.Count; i++)
                {
                    if (goodieGrow[i].Intersects(ball1.box))
                    {

                        OnGoodie?.Invoke(this, null);



                        ball1.box.SizeX = bigball1Size;
                        ball1.box.SizeY = bigball1Size * aspect;

                        if (goodieGrow[i].Y < ball1.box.Y && goodieGrow[i].X > ball1.box.X)
                        {
                            superCollide();
                        }
                        else
                        {
                            superCollideElse();
                        }
                        goodieGrow.RemoveAt(i);
                    }

                }


                for (int i = 0; i < goodieRemoveColumn.Count; i++)
                {

                    if (goodieRemoveColumn[i].Intersects(ball1.box))
                    {
                     
                        OnGoodie?.Invoke(this, null);

                        jeez = false;

                        if (goodieRemoveColumn[i].Y < ball1.box.Y && goodieRemoveColumn[i].X > ball1.box.X)
                        {
                            superCollide();
                        }
                        else
                        {
                            superCollideElse();
                        }
                    }

                    if (jeez == false)
                    {
                        float love = hitable.Count * 0.2f;

                        int what = (int)love;

                        for (int n = 0; n < what; n++)
                        {
                            Random rnd = new Random();
                            int me = rnd.Next(n, hitable.Count);

                            hitable.RemoveAt(me);

                        }

                        goodieRemoveColumn.RemoveAt(i);
                        jeez = true;
                    }
                }

                for (int i = 0; i < hitable.Count; i++)
                {
                    if (hitable[i].Intersects(theEnd))
                    {
                        lost = true;
                        if (timeStop == true)
                        {
                            Timer.Stop();
                            newTime = Timer.Elapsed;
                            timeStop = false;
                        }
                    }

                    if (hitable[i].Intersects(ball1.box) && (ball1.canCollide == true))
                    {
                        
                            animateMe = new Box2D(hitable[i].X, hitable[i].Y, hitable[i].SizeX, hitable[i].SizeY);
                            animatedHits.Add(animateMe);
                        
                        OnHit?.Invoke(this, null);
                        Box2D overlap = null;
                        overlap = new Box2D(0.0f, 0.0f, 0.0f, 0.0f);
                        ball1.dir.X *= -1;

                        if (hitable[i].Y < ball1.box.Y && hitable[i].X > ball1.box.X)
                        {
                            superCollide();

                        }
                        else
                        {
                            superCollide();
                        }

                        ball1.canCollide = false;

                        hitable.RemoveAt(i);
                        i--;



                        if (won == false)
                        {
                            hitpoint = hitpoint + 1;
                        }

                    }
                }
                ball1.canCollide = true;
                /*for (int i = 0; i < hitable.Count; i++)
                {
                    //if (hitable[i].Intersects(ball1.box) == false && (ball1.canCollide == false))
                    if ((ball1.canCollide == false))
                    {
                        changeDir();

                       
                    }
                }*/



                if (helpBall != null) // wenn helpball existiert
                {
                    for (int k = 0; k < hitable.Count; k++)
                    {

                        if (hitable[k].Intersects(helpBall.box))
                        {
                            animateMe = new Box2D(hitable[k].X, hitable[k].Y, hitable[k].SizeX, hitable[k].SizeY);
                            animatedHits.Add(animateMe);

                            OnHit?.Invoke(this, null);
                            hitable.RemoveAt(k);
                            k--;
                            helpBall.dir.Y = helpBall.dir.Y * (-1);
                            helpBall.canCollide = false;

                            if (won == false)
                            {
                                hitpoint = hitpoint + 1;
                            }

                        }
                    }
                }

                for (int i = 0; i < hitable.Count; i++)
                {
                    for (int j = 0; j < helpBallList.Count; j++)
                    {
                        if (j >= helpBallList.Count)
                        {
                            j--;
                        }
                        if (j >= helpBallList.Count)
                        {
                            j--;
                        }
                        if (hitable[i].Intersects(helpBallList[j].box))
                        {
                            animateMe = new Box2D(hitable[i].X, hitable[i].Y, hitable[i].SizeX, hitable[i].SizeY);
                            animatedHits.Add(animateMe);

                            OnHit?.Invoke(this, null);
                            hitable.RemoveAt(i);
                            i--;

                            helpBallList[j].dir.Y = helpBallList[j].dir.Y * (-1);
                            helpBallList[j].canCollide = false;

                            if (won == false)
                            {
                                hitpoint = hitpoint + 1;
                            }
                        }
                    }

                }

                //huhu

                if ((ball1.box.Y < -1) | (ball1.box.MaxY > 1))
                {
                    ball1.canCollide = true;
                    

                }
                if ((ball1.box.MaxX < -1) | (ball1.box.MaxX > 1))
                {
                    ball1.canCollide = true;
                }



                if (helpBall != null)
                {

                    if ((helpBall.box.MaxY > 1))
                    {
                        helpBall.canCollide = true;
                    }
                    if ((helpBall.box.MaxX < -1) | (helpBall.box.MaxX > 1))
                    {
                        helpBall.canCollide = true;
                    }
                    if (helpBall.box.MaxY < -1)
                    {
                        helpBall = null;
                    }

                }

                for (int i = 0; i < helpBallList.Count; i++)
                {
                    if (helpBallList[i].box.MaxY < -1)
                    {
                        helpBallList.RemoveAt(i);
                    }

                }

                if (hasNotStarted == true)
                {
                    if (won == false)
                    {
                        ball1.update();
                        if (helpBall != null)
                        {
                            helpBall.update();


                        }
                        for (int i = 0; i < helpBallList.Count; i++)
                        {
                            helpBallList[i].update();
                        }
                    }

                }

            }
            if (hitable.Count < 10)
            {
                won = true;
                if (timeStop == true)
                {
                    Timer.Stop();
                    newTime = Timer.Elapsed;
                    timeStop = false;
                }
            }
        }



        private void Render()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Color3(Color.White);
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.Blend);
            //GL.Color3(Color.CornflowerBlue);
            DrawComplex(player);
            DrawComplex(ball1.box);




            if (lost == false)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), background);

                if (helpBall != null)
                {
                    DrawComplex(helpBall.box);
                    DrawTexturedRect(helpBall.box, helpBallTex);
                 
                }
                for (int i = 0; i < helpBallList.Count; i++)
                {
                    DrawComplex(helpBallList[i].box);
                    DrawTexturedRect(helpBallList[i].box, helpBallTex);
                }



                for (int i = 0; i < goodieDoubleMe.Count; i++)
                {
                    DrawTexturedRect(goodieDoubleMe[i], doubleMeTex);
                }

                for (int i = 0; i < goodieGrow.Count; i++)
                {
                    DrawTexturedRect(goodieGrow[i], growTex);
                }

                for (int i = 0; i < addBadGoodies.Count; i++)
                {
                    DrawTexturedRect(addBadGoodies[i], fasterTex);
                }

                for (int i = 0; i < goodieMoreRows.Count; i++)
                {
                    DrawTexturedRect(goodieMoreRows[i], moreRowsTex);
                }

                for (int i = 0; i < goodieRemoveColumn.Count; i++)
                {
                    DrawTexturedRect(goodieRemoveColumn[i], removeColumnTex);
                }
                for (int i = 0; i < hitable.Count; i++)
                {
                    //timeSource.Start();
                    DrawTexturedRect(hitable[i], startAni);



                   
                        for (int j = 0; j < animatedHits.Count; j++)
                        {
                            eiscreme.Draw(animatedHits[j], (float)timeSource.Elapsed.TotalSeconds);
                            DrawBoxOutline(animatedHits[j]);
                            
                            timeSource.Start();


                            if ((float)timeSource.Elapsed.TotalSeconds > 0.9)
                            {
                                timeSource.Reset();


                                animatedHits.Clear();

                                

                            }
                        }
                    


                    

                }



                GL.LineWidth(2.0f);
                GL.Color3(Color.White);
                DrawTexturedRect(player, playerTex);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                GL.Enable(EnableCap.Blend);
                DrawTexturedRect(ball1.box, ballTex);
                
                for (int i = 0; i < hitable.Count; i++)
                {
                    DrawBoxOutline(hitable[i]);

                    //DrawTexturedRect(hitable[i], TextureLoader.FromBitmap(Resourcen.Eis1));
                }
                for (int i = 0; i < goodieRemoveColumn.Count; i++)
                {
                    DrawBoxOutline(goodieRemoveColumn[i]);
                }
                for (int i = 0; i < goodieMoreRows.Count; i++)
                {
                    DrawBoxOutline(goodieMoreRows[i]);
                }
                for (int i = 0; i < addBadGoodies.Count; i++)
                {
                    DrawBoxOutline(addBadGoodies[i]);
                }
                for (int i = 0; i < goodieGrow.Count; i++)
                {
                    DrawBoxOutline(goodieGrow[i]);
                }
                for (int i = 0; i < goodieDoubleMe.Count; i++)
                {
                    DrawBoxOutline(goodieDoubleMe[i]);
                }
                
                if (withPoints == true)
                {
                    Writing();
                }

                if (withTime == true)
                {
                    Writing4();
                                       
                }
            }

            if (lost == true)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), background);
                Writing2();

                if (Keyboard.GetState()[Key.Space])
                {
                    restart();
                }

            }

            if (hasNotStarted == false)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), startTex);
                

                if (Keyboard.GetState()[Key.Left])
                {
                    Timer.Start();
                    hasNotStarted = true;
                    withTime = true;
                }

                if (Keyboard.GetState()[Key.Right])
                { 
                    hasNotStarted = true;
                    withPoints = true;
                }
            }

            if (won == true)
            {
                DrawTexturedRect(new Box2D(-1, -1, 2, 2), youWonTex);
                Writing3();


                if (Keyboard.GetState()[Key.Space])
                {
                    
                    restart();
                }
            }

            if (ball1.box.Y < -0.96f)
            {
                lost = true;

            }

            if (currentTime.TotalSeconds < 0)
            {
                won = true;
            }

            currentTime.ToString();

            string formattedTimespan = currentTime.ToString("hh\\:mm");
        }



        private static void DrawTexturedRect(Box2D Rect, Texture tex)
        {
            tex.Activate();
            GL.Begin(PrimitiveType.Quads);
            //when using textures we have to set a texture coordinate for each vertex
            //by using the TexCoord command BEFORE the Vertex command
            GL.TexCoord2(0.0f, 0.0f); GL.Vertex2(Rect.X, Rect.Y);
            GL.TexCoord2(1.0f, 0.0f); GL.Vertex2(Rect.MaxX, Rect.Y);
            GL.TexCoord2(1.0f, 1.0f); GL.Vertex2(Rect.MaxX, Rect.MaxY);
            GL.TexCoord2(0.0f, 1.0f); GL.Vertex2(Rect.X, Rect.MaxY);
            GL.End();
            //the texture is disabled, so no other draw calls use this texture
            tex.Deactivate();
        }

        private void DrawBoxOutline(Box2D rect)
        {
            GL.Begin(PrimitiveType.LineLoop);
            GL.Vertex2(rect.X, rect.Y);
            GL.Vertex2(rect.MaxX, rect.Y);
            GL.Vertex2(rect.MaxX, rect.MaxY);
            GL.Vertex2(rect.X, rect.MaxY);
            GL.End();
        }

        private void DrawComplex(Box2D rect)
        {
            var xQuarter = rect.X + rect.SizeX * 0.25f;
            var x3Quarter = rect.X + rect.SizeX * 0.75f;
            var yThird = rect.Y + rect.SizeY * 0.33f;
            var y2Third = rect.Y + rect.SizeY * 0.66f;
            GL.Begin(PrimitiveType.Polygon);
            GL.Vertex2(rect.CenterX, rect.MaxY);
            GL.Vertex2(x3Quarter, y2Third);
            GL.Vertex2(rect.MaxX, rect.CenterY);
            GL.Vertex2(x3Quarter, yThird);
            GL.Vertex2(rect.MaxX, rect.Y);
            GL.Vertex2(rect.CenterX, yThird);
            GL.Vertex2(rect.X, rect.Y);
            GL.Vertex2(xQuarter, yThird);
            GL.Vertex2(rect.X, rect.CenterY);
            GL.Vertex2(xQuarter, y2Third);
            GL.End();
        }

        private void Writing()
        {
            GL.Color3(Color.White);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            font.Print(-1f, onScreenFontPlace, 0, onScreenFontSize, hitpoint.ToString());

            if (showExtraPoints == true)
            {   
                font.Print(-0.9f, 0f, 0, .2f, "Extra Points!");     
            }

            GL.Disable(EnableCap.Blend);
        }

        private void Writing2()
        {
            GL.Color3(Color.White);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            font.Print(-1f, 0.3f, 0, .3f, "You lost!");

            if (withPoints == true)
            {
                font.Print(-0.4f, 0f, 0, 0.2f, "Score:");

                if (hitpoint < 10)
                {
                    font.Print(-0.25f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }

                if (hitpoint < 100 && hitpoint >= 10)
                {
                    font.Print(-0.45f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }

                if (hitpoint >= 100)
                {
                    font.Print(-0.6f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }
            }

            if (withTime == true)
            {
                font.Print(-0.35f, 0f, 0, 0.2f, "Time:");
                font.Print(-0.35f, -0.3f, 0, .34f, currentTime.ToString("ss"));
                
            }

            font.Print(-0.9f, -0.5f, 0, .115f, "Press space to restart");
            GL.Disable(EnableCap.Blend);
        }

        private void Writing3()
        {
            GL.Color3(Color.White);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            font.Print(-1f, 0.3f, 0, .35f, "You won!");

            if (withPoints == true)
            {
                font.Print(-0.4f, 0f, 0, 0.2f, "Score:");

                if (hitpoint < 10)
                {
                    font.Print(-0.25f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }

                if (hitpoint < 100 && hitpoint >=10)
                {
                    font.Print(-0.45f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }

                if (hitpoint >= 100)
                {
                    font.Print(-0.6f, -0.45f, 0, 0.5f, hitpoint.ToString());
                }
            }

            if (withTime == true)
            {
                font.Print(-0.35f, 0f, 0, 0.2f, "Time:");
                font.Print(-1f, -0.3f, 0, .34f, currentTime.ToString("ss"));
            }

            font.Print(-0.9f, -0.5f, 0, .115f, "Press space to restart");
            GL.Disable(EnableCap.Blend);
        }

        private void Writing4()
        {
            GL.Color3(Color.White);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            font.Print(0.745f, onScreenFontPlace, 0, onScreenFontSize, currentTime.ToString("ss"));
            GL.Disable(EnableCap.Blend);
        }


        [STAThread]
        private static void Main()
        {
            app = new ExampleApplication(480, 760);
            app.GameWindow.Location = new Point(50, 20);
            controller = new Controller();
            app.Render += controller.Render;
            app.Update += controller.Update;
            controller.OnShoot += (sender, args) => { controller.sound.Shoot(); };
            controller.OnBadGoodie += (sender, args) => { controller.sound.BadGoodie(); };
            controller.OnGoodie += (sender, args) => { controller.sound.Goodie(); };
            controller.OnHit += (sender, args) => { controller.sound.Hit(); };
            controller.OnBackground += (sender, args) => { controller.sound.Background(); };
            app.Run();


        }

        public void restart()
        {
            app.Render -= controller.Render;
            app.Update -= controller.Update;


            // Alles neu
            controller = new Controller();
            app.Render += controller.Render;
            app.Update += controller.Update;
            controller.OnShoot += (sender, args) => { controller.sound.Shoot(); };
            controller.OnBadGoodie += (sender, args) => { controller.sound.BadGoodie(); };
            controller.OnGoodie += (sender, args) => { controller.sound.Goodie(); };
            controller.OnHit += (sender, args) => { controller.sound.Hit(); };

        }

    }
}